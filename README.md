# 02 Multiple Environments

This project illustrates a very basic Terraform setup
which uses the [GitLab-managed Terraform state](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html)
and the [GitLab Terraform CI/CD template](https://docs.gitlab.com/ee/user/infrastructure/iac/#latest-terraform-template)
to provision multiple environments: `production`, `staging` and `testing`.

Each environment:

- shares the same Terraform config.
- is configured using its own separate `.tfvars` file in the `vars/` directory.
- is triggered in its own child-pipeline.
- has its own Terraform state.
- has its own GitLab Environment.