variable "pet_name_length" {
  type        = number
  description = "The word length for the pet name."
}